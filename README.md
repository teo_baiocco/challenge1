# Java Spring Basic Challenge

This example will allow us to see your development for a simple micro-service.

There is no timeframe defined for this, but we understand you will take no longer than 6 hours to do these tasks.

Also, there is no need to do all tasks, we want to assess your ability to develop, test, solve problems and be pro-active, beyond just code.

## Tasks

### 1. Create a micro-service to be run with Gradle or Maven

You can use [Spring Initializr][SpringInitializr] to kick-start, or do directly here. The dependencies are free choice, use those you like, know best.
After created, your service must have a URI that shows it is running, like `/health`

### 2. Integrate OpenWeatherMap and make it operational

You will use OpenWeatherMaps to complete this service, so take a time to make your personal registration to get your API Key.
Go to [OpenWeatherMaps][OpenWeather] for this. We also recomend you to check carefully the documentation there.

Your goal is to have an endpoint `GET /weather` that accepts the parameter `city` and return weather data to you city, with details like (condition, temperatura, atmospheric pression, etc). All that through HTTP requests made to OpenWeatherMaps.

You can organize your response as you like most. But we suggest you to use a library to treat errors and serialization/deserialization for you.

Structure your outcome in a way you think more suitable... There's no stricted rule, be creative!


### 3. Security

We want to limit the access to your services also using an API key to avoid unrequested calls.
So, implement Spring security strategy to use an API key you'll set and share with allowed clients.

### 4. Testing the API service

How can we test these call?
Document, explain how to clients will connect. May you create or use a library to make your service self testable?

### 5. Improvements

Any improvements you want to do on that specification is welcomed.
Know how to use caching? Do it.
Want to store locally some data to respond even when OpenWeatherMaps is offline? Store it.
Would you like to use Docker to run? Use it.

[OpenWeather]: https://openweathermap.org/appid
[SpringInitializr]: https://start.spring.io/
[SpringBoot]: https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#getting-started